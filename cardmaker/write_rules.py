
data = {
  "v": 1,
  "data": [
    {
      "id": "d1",
      "matchUrl": "https://burgertokens.com/pages/keyforge-card-builder",
      "rules": [],
      "on": True
    }
  ]
}

#        {
#          "type": "normalOverride",
#          "match": "https://cardbuilder.burgertokens.com/static/keyforge/sanctum/sanctum_faction_icon.png",
#          "replace": "http://127.0.0.1:8000/bavaria/logo.png",
#          "on": True
#        },

houses = {}
houses['sanctum'] = 'bavaria'
houses['shadow'] = 'la_trappe_quadrupel'
houses['saurian_republic'] = 'la_chouffe'

urls = {}

for h, b in houses.items():
    n = "shadows" if h == 'shadow' else "brobnar" if h == "saurian_republic" else h

    l = "{}_".format(h) if h != 'saurian_republic' else ''

    urls["{}/{}faction_icon.png".format(h, l)] = "{}/res/logo.png".format(b)
    urls["archon/{}_faction_icon.png".format(n)] = "{}/res/archon.png".format(b)
    urls["archon/{}_label.png".format(n)] = "{}/res/label.png".format(b)

    for bg in ["action", "upgrade", "character", "artifact"]:
      bg2 = bg if bg != "character" else "creature"
      urls["{}/{}_text_background.png".format(h,bg)] = "{}/res/{}_text_background.png".format(b,bg2)

urls["text_icon_aember.png"] = "other_icons/malt.png"

for u, u2 in urls.items():
  baseurl = "https://cardbuilder.burgertokens.com/static/keyforge"
  baseurl2 = "http://127.0.0.1:8000"
  obj = {"type": "normalOverride", "match": "{}/{}".format(baseurl, u), "replace": "{}/{}".format(baseurl2, u2), "on": True}

  data['data'][0]['rules'].append(obj)


import json
with open('resource_override_rules.json', 'w') as json_file:
  json.dump(data, json_file)
